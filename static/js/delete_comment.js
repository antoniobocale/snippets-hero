let deleteComment = document.getElementById('deleteComment')
deleteComment.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    let button = event.relatedTarget
    // Extract info from data-bs-* attributes
    let comment_delete = button.getAttribute('data-bs-comment-delete')
    // If necessary, you could initiate an AJAX request here
    // and then do the updating in a callback.
    //
    // Update the modal's content.
    let deleteCommentForm = deleteComment.querySelector('#deleteCommentForm')
    deleteCommentForm.action = comment_delete

})

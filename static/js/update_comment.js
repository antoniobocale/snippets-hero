let updateComment = document.getElementById('updateComment')
updateComment.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    let button = event.relatedTarget
    // Extract info from data-bs-* attributes
    let comment_update = button.getAttribute('data-bs-comment-update')
    let comment = button.getAttribute('data-bs-comment')
    // If necessary, you could initiate an AJAX request here
    // and then do the updating in a callback.
    //
    // Update the modal's content.
    let updateCommentForm = updateComment.querySelector('#updateCommentForm')
    updateCommentForm.action = comment_update
    let commentInput = updateComment.querySelector('#comment-content')
    commentInput.value = comment

})

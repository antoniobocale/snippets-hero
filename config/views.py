from django.db.models import Max, Sum, Count
from django.db.models.functions import Coalesce
from django.views.generic import TemplateView

from snippetshero.models import Snippet, UserProfile, Tag


class Home(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            top_snippets = Snippet.objects.order_by('-stars', '-create_on')[:10]
            recent_snippets = Snippet.objects.order_by('-create_on', 'title')[:10]
            users_with_stars = UserProfile.objects.annotate(total_stars=Coalesce(Sum('snippets__stars'), 0))
            snippets_heroes = users_with_stars.order_by('-total_stars', '-date_joined')[:10]
            popular_tags = Tag.objects.annotate(times=Count('snippet')).order_by('-times')[:10]
            context['top_snippets'] = top_snippets
            context['recent_snippets'] = recent_snippets
            context['snippets_heroes'] = snippets_heroes
            context['popular_tags'] = popular_tags
        except KeyError:
            pass
        return context

"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LogoutView, PasswordChangeView, PasswordChangeDoneView
from django.urls import include, path

from config.views import Home

from snippetshero.views import LanguagesList, UserCreateView, UserDetail, UserLoginView, SnippetsByTag, UserUpdate, \
    ChangeUserPrivacy, ChangeUserPassword, SnippetsSearchView

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('admin/', admin.site.urls),
    path('snippets/', include('snippetshero.urls')),
    path('languages/', LanguagesList.as_view(), name='languages-list'),
    path('languages/<str:language>', LanguagesList.as_view(), name='language-snippets'),
    path('signup', UserCreateView.as_view(), name='user-create'),
    path('login', UserLoginView.as_view(), name='user-login'),
    path('logout', LogoutView.as_view(), name='user-logout'),
    path('snippetshero_user/<int:pk>', UserDetail.as_view(), name='user-detail'),
    path('snippetshero_user/<int:pk>/update', UserUpdate.as_view(), name='user-update'),
    path('snippetshero_user/<int:pk>/update-privacy', ChangeUserPrivacy.as_view(), name='user-update-privacy'),
    path('tags/<str:tag>', SnippetsByTag.as_view(), name='snippets-by-tag'),
    path('password_change/', ChangeUserPassword.as_view(), name='password_change'),
    path('search/', SnippetsSearchView.as_view(), name='search')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from snippetshero.models import Snippet, UserProfile, Language, Comment, Tag


# Register your models here.


@admin.register(Snippet)
class SnippetAdmin(admin.ModelAdmin):
    list_display = ('author', 'title', 'description', 'language', 'stars', 'create_on')
    list_filter = ('language',)
    search_fields = ('title__icontains', 'author__username__icontains')


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title__icontains',)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('snippet', 'author', 'content', 'create_on')
    search_fields = ('snippet__title__icontains', 'content__icontains', 'author__icontains')


@admin.register(UserProfile)
class UserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'username', 'email', 'is_superuser', 'is_staff', 'is_active', 'private')
    search_fields = ('first_name', 'last_name', 'username')
    list_filter = ('is_superuser', 'is_staff', 'is_active', 'private')


admin.site.register(Tag)

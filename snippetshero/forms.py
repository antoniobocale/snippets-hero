import django.forms
from crispy_bootstrap5.bootstrap5 import FloatingField
from crispy_forms.bootstrap import FormActions, PrependedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, HTML, Div
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UserChangeForm, PasswordChangeForm
from django.urls import reverse_lazy

from snippetshero.models import Snippet, UserProfile, Language, Comment


class SnippetForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_show_labels = False

    helper.layout = Layout(
        HTML('<h5>Title</h3>'),
        Field('title'),
        HTML('<h5>Description</h3>'),
        Field('description'),
        HTML('<h5>Language</h3>'),
        Field('language'),
        HTML('<h5>Code</h3>'),
        Field('code'),
        HTML('<h5>Tags</h3>'),
        HTML('<input type="text" name="tags" class="form-control tagin">'
             '<script> tagin( document.querySelector(\'.tagin\') ); </script>'),
        FormActions(
            Submit('save', 'Confirm', css_class='btn-warning'),
            HTML(f'<a class="btn btn-outline-dark" onclick="moveToReferer();" role="button">Cancel</a>'),
            css_class='my-3'
        )
    )

    class Meta:
        model = Snippet
        fields = ['title', 'description', 'language', 'code']


class UserCreateForm(UserCreationForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_show_labels = False

    helper.layout = Layout(
        Div(Div(FloatingField('first_name'), css_class='col-12 col-lg-6'),
            Div(FloatingField('last_name'), css_class='col-12 col-lg-6'),
            css_class='row row-cols-1 row-cols-lg-3 gy-1 gx-3'),
        FloatingField('username'),
        FloatingField('email'),
        PrependedText('image', ''),
        FloatingField('password1'),
        FloatingField('password2'),
        FormActions(
            Submit('save', 'Sign-up', css_class='btn-warning me-3'),
            HTML(f'<a class="btn btn-outline-dark" href="/" role="button">Cancel</a>')
        )
    )

    class Meta(UserCreationForm):
        model = UserProfile
        fields = ['first_name', 'last_name', 'username', 'email', 'image']
        success_url = reverse_lazy('home')

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.pop('autofocus', None)
        self.fields['image'].help_text = 'Select a profile image.'


class UserLoginForm(AuthenticationForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_show_labels = False

    helper.layout = Layout(
        FloatingField('username'),
        FloatingField('password'),
        FormActions(
            Submit('save', 'Login', css_class='col-12 btn-warning me-3 mb-1'),
            HTML(f'<a class="col-12 btn btn-dark" href="/" role="button">Cancel</a>')
        )
    )

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.pop('autofocus', None)


class UserUpdateForm(UserChangeForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_show_labels = False

    helper.layout = Layout(
        HTML('<h5>First name</h3>'),
        Field('first_name'),
        HTML('<h5>Last name</h3>'),
        Field('last_name'),
        HTML('<h5>Username</h3>'),
        Field('username'),
        HTML('<h5>Email</h3>'),
        Field('email'),
        HTML('<h5>Image</h3>'),
        Field('image'),
        FormActions(
            Submit('save', 'Update', css_class='btn-warning'),
            HTML(f'<a class="btn btn-outline-dark" href="/" role="button">Cancel</a>'),
            css_class='my-3'
        )
    )

    class Meta(UserChangeForm):
        model = UserProfile
        fields = ['first_name', 'last_name', 'username', 'email', 'image']
        success_url = reverse_lazy('home')


class ChangePasswordForm(PasswordChangeForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_show_labels = False

    helper.layout = Layout(
        FloatingField('old_password'),
        FloatingField('new_password1'),
        FloatingField('new_password2'),
        FormActions(
            Submit('save', 'Confirm', css_class='btn-warning'),
            HTML(f'<a class="btn btn-outline-dark" href="/" role="button">Cancel</a>'),
            css_class='my-3'
        )
    )

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['old_password'].widget.attrs.pop('autofocus', None)


class SnippetsSearchForm(forms.Form):
    username = forms.CharField(required=False)
    min_date = forms.DateTimeField(required=False, widget=forms.TextInput(attrs={'type': 'date'}))
    max_date = forms.DateTimeField(required=False, widget=forms.TextInput(attrs={'type': 'date'}))
    language = forms.ModelChoiceField(Language.objects.all(), required=False, to_field_name='value')
    helper = FormHelper()
    helper.form_method = 'GET'
    helper.form_show_labels = False

    helper.layout = Layout(
        Div(Div(FloatingField('username'), css_class='col-12 col-lg-4'),
            Div(FloatingField('min_date'), css_class='col-12 col-lg-4'),
            Div(FloatingField('max_date'), css_class='col-12 col-lg-4'),
            css_class='row row-cols-1 row-cols-lg-3 gy-1 gx-3'),
        FloatingField('language'),
        HTML('<div id="div_id_tags" class="form-floating mb-3">'
             '<input id="id_tags" class="textinput textInput form-control tagin" type="text" name="tags" placeholder="tags">'
             '<label for="id_tags">Tags</label>'
             '<script> tagin( document.querySelector(\'.tagin\') ); </script></div>'),
        FormActions(
            Submit('search', 'Search', css_class='btn-warning'),
        )
    )


class CommentForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_show_labels = False

    class Meta:
        model = Comment
        fields = ['content']

    def __init__(self, *args, action=None, submit_label='Update comment', **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)
        self.fields['content'].label = "Comment"
        CommentForm.helper.form_action = action
        CommentForm.helper.layout = Layout(
            FloatingField('content'),
            FormActions(
                Submit('confirm', submit_label, css_class='btn-warning'),
            )
        )


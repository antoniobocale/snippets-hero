from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView, FormView
from snippetshero.forms import SnippetForm, UserCreateForm, UserLoginForm, UserUpdateForm, ChangePasswordForm, \
    SnippetsSearchForm, CommentForm
from snippetshero.models import Snippet, Language, UserProfile, Tag, Comment


class SnippetCreate(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Snippet
    template_name = 'snippetshero/snippets/snippet_create.html'
    success_url = reverse_lazy('home')
    form_class = SnippetForm
    success_message = 'Snippet created successfully!'

    def form_valid(self, form):
        form.instance.author = self.request.user
        _ = super().form_valid(form)
        tag_string = self.request.POST.get('tags')
        if tag_string != '':
            tags = tag_string.split(',')
            for tag_name in tags:
                try:
                    tag = Tag.objects.create(name=tag_name)
                except IntegrityError:
                    tag = Tag.objects.get(name=tag_name)
                self.object.tags.add(tag)
        self.success_url = reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.object.pk})
        return HttpResponseRedirect(self.get_success_url())


class SnippetUpdate(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Snippet
    template_name = 'snippetshero/snippets/snippet_update.html'
    form_class = SnippetForm
    success_message = 'Snippet updated successfully!'

    def get(self, request, *args, **kwargs):
        self.success_url = reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']})
        if not self.permission_granted(request):
            return HttpResponseRedirect(self.success_url)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.success_url = reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']})
        if not self.permission_granted(request):
            return HttpResponseRedirect(self.success_url)
        return super().post(request, *args, **kwargs)

    def permission_granted(self, request):
        snippet = Snippet.objects.get(pk=self.kwargs['pk'])
        if request.user.pk == snippet.author.pk:
            return True
        return False

    def form_valid(self, form):
        form.instance.author = self.request.user
        response = super().form_valid(form)
        tag_string = self.request.POST.get('tags')
        if tag_string != '':
            tags = tag_string.split(',')
            for tag_name in tags:
                try:
                    tag = Tag.objects.create(name=tag_name)
                except IntegrityError:
                    tag = Tag.objects.get(name=tag_name)
                self.object.tags.add(tag)
        return response


class SnippetDetail(DetailView):
    model = Snippet
    template_name = 'snippetshero/snippets/snippet_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CommentForm(action=reverse_lazy('snippetshero:create-comment', kwargs={'pk': self.kwargs['pk']}), submit_label='Add comment')
        return context


class SnippetDelete(LoginRequiredMixin, DeleteView):
    model = Snippet
    success_url = reverse_lazy('home')
    success_message = 'Snippet deleted successfully!'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']}))


class LanguagesList(ListView):
    model = Language
    template_name = 'snippetshero/languages.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            language = Language.objects.get(value=self.kwargs['language'])
            context['snippets'] = Snippet.objects.filter(language=language)
            context['language'] = language
        except KeyError:
            pass
        return context


class UserCreateView(SuccessMessageMixin, CreateView):
    form_class = UserCreateForm
    template_name = 'snippetshero/registration/signup.html'
    success_url = reverse_lazy('home')
    success_message = "User created successfully!"


class UserLoginView(LoginView):
    form_class = UserLoginForm
    template_name = 'snippetshero/registration/login.html'
    success_url = reverse_lazy('home')


class SnippetLikeView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']}))

    def post(self, request, *args, **kwargs):
        snippet = Snippet.objects.get(pk=self.kwargs['pk'])
        if request.user.liked.filter(pk=snippet.pk).count() == 0:
            request.user.liked.add(snippet)
            snippet.stars += 1
        else:
            request.user.liked.remove(snippet)
            snippet.stars -= 1
        snippet.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class SnippetSaveView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']}))

    def post(self, request, *args, **kwargs):
        snippet = Snippet.objects.get(pk=self.kwargs['pk'])
        if request.user.saved.filter(pk=snippet.pk).count() == 0:
            request.user.saved.add(snippet)
        else:
            request.user.saved.remove(snippet)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class UserDetail(DetailView):
    model = UserProfile
    template_name = 'snippetshero/snippetshero_user/user_detail.html'


class SnippetRemoveTagView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']}))

    def post(self, request, *args, **kwargs):
        snippet = Snippet.objects.get(pk=self.kwargs['pk'])
        tag = Tag.objects.get(name=self.kwargs['tag_name'])
        snippet.tags.remove(tag)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class SnippetsByTag(ListView):
    model = Snippet
    template_name = 'snippetshero/snippets/snippets_by_tag.html'

    def get_queryset(self):
        tag_name = self.kwargs['tag']
        tag = Tag.objects.get(name=tag_name)
        snippets = tag.snippet_set.all()
        return snippets

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tag_name'] = self.kwargs['tag']
        return context


class UserUpdate(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = UserProfile
    template_name = 'snippetshero/snippetshero_user/user_update.html'
    form_class = UserUpdateForm
    success_message = 'User updated successfully!'

    def get(self, request, *args, **kwargs):
        self.success_url = reverse_lazy('user-detail', kwargs={'pk': self.kwargs['pk']})
        if not self.permission_granted(request):
            return HttpResponseRedirect(self.success_url)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.success_url = reverse_lazy('user-detail', kwargs={'pk': self.kwargs['pk']})
        if not self.permission_granted(request):
            return HttpResponseRedirect(self.success_url)
        return super().post(request, *args, **kwargs)

    def permission_granted(self, request):
        user = UserProfile.objects.get(pk=self.kwargs['pk'])
        if request.user.pk == user.pk:
            return True
        return False


class ChangeUserPrivacy(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('user-detail', kwargs={'pk': self.kwargs['pk']}))

    def post(self, request, *args, **kwargs):
        user = UserProfile.objects.get(pk=self.kwargs['pk'])
        if request.user.pk == user.pk:
            user.private = not user.private
        user.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class ChangeUserPassword(SuccessMessageMixin, PasswordChangeView):
    form_class = ChangePasswordForm
    template_name = 'snippetshero/registration/password_change_form.html'
    success_message = 'Password changed successfully!'

    def post(self, request, *args, **kwargs):
        self.success_url = reverse_lazy('user-detail', kwargs={'pk': request.user.pk})
        return super().post(request, *args, **kwargs)


class SnippetsSearchView(FormView):
    model = Snippet
    template_name = 'snippetshero/search.html'
    form_class = SnippetsSearchForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        snippets = Snippet.objects.all()
        username = self.request.GET.get('username')
        min_date = self.request.GET.get('min_date')
        max_date = self.request.GET.get('max_date')
        language = self.request.GET.get('language')
        tags = self.request.GET.get('tags')
        params = [username, min_date, max_date, language, tags]
        if all(param in [None, ''] for param in params):
            return context
        if username not in [None, '']:
            snippets = snippets.filter(author__username=username)
        if min_date not in [None, '']:
            snippets = snippets.filter(create_on__gte=min_date)
        if max_date not in [None, '']:
            snippets = snippets.filter(create_on__lte=max_date)
        if language not in [None, '']:
            snippets = snippets.filter(language__value=language)
        if tags not in [None, '']:
            snippets = snippets.filter(tags__name__in=tags.split(','))
        context['snippets'] = snippets
        return context


class CreateCommentView(SuccessMessageMixin, LoginRequiredMixin, View):
    success_message = 'Comment created successfully!'

    def get(self, request, *args, **kwargs):
        snippet_pk = self.kwargs['pk']
        return HttpResponseRedirect(reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': snippet_pk}))

    def post(self, request, *args, **kwargs):
        form = CommentForm(request.POST)
        form.instance.author = self.request.user
        form.instance.snippet = Snippet.objects.get(pk=self.kwargs['pk'])
        if form.is_valid():
            form.save()
            messages.success(self.request, self.success_message)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class CommentDelete(LoginRequiredMixin, DeleteView):
    model = Comment
    success_message = 'Comment deleted successfully!'

    def get_object(self, queryset=None):
        return Comment.objects.get(pk=self.kwargs['comment'])

    def delete(self, request, *args, **kwargs):
        self.success_url = reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']})
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']}))


class CommentUpdate(LoginRequiredMixin, UpdateView):
    model = Comment
    template_name = 'snippetshero/comments/comment_update.html'
    success_message = 'Comment updated successfully!'
    form_class = CommentForm

    def get_object(self, queryset=None):
        return Comment.objects.get(pk=self.kwargs['comment'])

    def post(self, request, *args, **kwargs):
        self.success_url = reverse_lazy('snippetshero:snippet-detail', kwargs={'pk': self.kwargs['pk']})
        messages.success(self.request, self.success_message)
        return super().post(request, *args, **kwargs)
from django.urls import path
from snippetshero.views import SnippetDetail, SnippetCreate, SnippetUpdate, \
    SnippetLikeView, SnippetSaveView, SnippetDelete, SnippetRemoveTagView, CreateCommentView, CommentDelete, \
    CommentUpdate

app_name = 'snippetshero'

urlpatterns = [
    path('create', SnippetCreate.as_view(), name='snippet-create'),
    path('<int:pk>/detail', SnippetDetail.as_view(), name='snippet-detail'),
    path('<int:pk>/add-comment', CreateCommentView.as_view(), name='create-comment'),
    path('<int:pk>/comments/<int:comment>/update', CommentUpdate.as_view(), name='update-comment'),
    path('<int:pk>/comments/<int:comment>/delete', CommentDelete.as_view(), name='delete-comment'),
    path('<int:pk>/update', SnippetUpdate.as_view(), name='snippet-update'),
    path('<int:pk>/delete', SnippetDelete.as_view(), name='snippet-delete'),
    path('<int:pk>/like', SnippetLikeView.as_view(), name='snippet-like'),
    path('<int:pk>/save', SnippetSaveView.as_view(), name='snippet-save'),
    path('<int:pk>/tags/<str:tag_name>/remove', SnippetRemoveTagView.as_view(), name='tag-remove')
]

from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.templatetags.static import static


class Tag(models.Model):
    name = models.CharField(max_length=50, unique=True)


class Language(models.Model):
    title = models.CharField(max_length=30, unique=True)
    value = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f'{self.title}'

    class Meta:
        ordering = ['title']


class Snippet(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='snippets', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    language = models.ForeignKey(Language, on_delete=models.PROTECT)
    code = models.TextField()
    stars = models.IntegerField(default=0)
    tags = models.ManyToManyField(Tag, blank=True)
    create_on = models.DateTimeField(auto_now_add=True, editable=False)

    # class Meta:
    #    verbose_name_plural = 'Snippets'

    def __str__(self):
        return f"{self.title} - id: {self.id}"


class UserProfile(AbstractUser):
    first_name = models.CharField(_('first name'), max_length=150)
    last_name = models.CharField(_('last name'), max_length=150)
    email = models.EmailField(_('email address'), unique=True)
    saved = models.ManyToManyField(Snippet)
    liked = models.ManyToManyField(Snippet, related_name='likes')
    image = models.ImageField(upload_to='profile_images', null=True, blank=True, default=None)
    private = models.BooleanField(default=False)

    def get_profile_image(self):
        try:
            return self.image.url
        except ValueError:
            return static('img/user.svg')

    def get_ranking(self):
        snippets = Snippet.objects.filter(author_id=self.pk)
        return sum(snippet.stars for snippet in snippets)


class Comment(models.Model):
    snippet = models.ForeignKey(Snippet, on_delete=models.CASCADE)
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    content = models.TextField()
    create_on = models.DateTimeField(auto_now_add=True, editable=False)
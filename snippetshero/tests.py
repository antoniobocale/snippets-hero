from django.test import TestCase

# Create your tests here.
from django.urls import reverse

from snippetshero.models import UserProfile, Language, Snippet


class UserTestCase(TestCase):
    def setUp(self):
        self.user_example = {
            'first_name': 'Antonio',
            'last_name': 'Bocale',
            'username': 'antoniobocale',
            'email': 'antoniobocale@gmail.com',
        }
        UserProfile.objects.create_user(**self.user_example, password='prova123')

    def test_create_user(self):
        response = self.client.post(reverse('user-create'), {
            'first_name': 'Victoria',
            'last_name': 'Blake',
            'username': 'vblake',
            'email': 'vblake@gmail.com',
            'password1' : 'prova123',
            'password2' : 'prova123'
        }, follow=True)
        self.assertRedirects(response, reverse('home'))
        self.assertContains(response, 'User created successfully!')
        self.assertIsNotNone(UserProfile.objects.get(username='vblake'))

    def test_create_existing_user(self):
        self.user_example['password1'] = 'prova123'
        self.user_example['password2'] = 'prova123'
        response = self.client.post(reverse('user-create'), self.user_example)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'A user with that username already exists.')
        self.assertContains(response, 'User with this Email address already exists.')

    def test_user_login(self):
        response = self.client.login(username='antoniobocale', password='prova123')
        self.assertTrue(response)

    def test_user_login_with_wrong_credentials(self):
        response = self.client.login(username='ng_denny', password='prova123')
        self.assertFalse(response)


class SnippetTestCase(TestCase):
    def setUp(self):
        self.user_example = {
            'first_name': 'Antonio',
            'last_name': 'Bocale',
            'username': 'antoniobocale',
            'email': 'antoniobocale@gmail.com',
            'password' : 'prova123'
        }
        UserProfile.objects.create_user(**self.user_example)
        Language.objects.create(title='Python', value='python')
        self.client.login(username='antoniobocale', password='prova123')
        self.snippet = {
            'title': 'Hello World',
            'description': 'Hello World in Python',
            'language': f'{Language.objects.get(value="python").id}',
            'code': 'print("Hello World")',
            'tags': ''
        }

    def create_snippet(self):
        return Snippet.objects.create(
            title='Hello World',
            description='Hello World in Python',
            language=Language.objects.get(value='python'),
            code='print("Hello World")',
            author=UserProfile.objects.get(username='antoniobocale')
        )

    def test_create_snippet(self):
        response = self.client.post(reverse('snippetshero:snippet-create'), self.snippet, follow=True)
        self.assertContains(response, 'Snippet created successfully!')
        user = response.context['user']
        self.assertEqual(Snippet.objects.filter(author=user).count(), 1)

    def test_read_snippet(self):
        snippet = self.create_snippet()
        response = self.client.get(reverse('snippetshero:snippet-detail', kwargs={'pk': snippet.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, snippet.title)

    def test_update_snippet(self):
        snippet = self.create_snippet()
        self.snippet['title'] = 'Updated Hello World'
        response = self.client.post(reverse('snippetshero:snippet-update', kwargs={'pk': snippet.pk}), self.snippet, follow=True)
        self.assertContains(response, 'Snippet updated successfully!')
        snippet = Snippet.objects.get(id=snippet.id)
        self.assertEqual(snippet.title, 'Updated Hello World')

    def test_delete_snippet(self):
        snippet = self.create_snippet()
        response = self.client.post(reverse('snippetshero:snippet-delete', kwargs={'pk': snippet.pk}), follow=True)
        self.assertRedirects(response, reverse('home'))
        self.assertContains(response, 'Snippet deleted successfully')
        with self.assertRaises(Snippet.objects.model.DoesNotExist):
            Snippet.objects.get(id=snippet.id)
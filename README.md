## Installazione e avvio

Una volta entrati nella directory del progetto, creare l’environment e installare i package necessari con:

`pipenv install`

A questo punto attivare l’environment con:

`pipenv shell`

Avviare il server con:

`python manage.py runserver`

Andare sul browser e connettersi a localhost:8000

# Login

È presente un account con username e password 'admin' in grado di accedere al pannello di amministrazione django.

La password dei seguenti account è 'snippets@Hero':

betty_dove

brooks44

cantu99

ladd94

maryg

ng_denny

ron_riv

silva89

vblake

wise_willy

